# Tweets Simply

![tweets simply](TweetsSimply.png)

Tweets Simply[^1] is a very simple Twitter client for the Mac.

It is based on mobile twitter web and supports all feature of the mobile twitter app including bookmarks and multiple photos.

## Will be added on the next release (I HOPE)

1. Reimplemented tweet button
1. Right click actions
1. Save images

## Changes

### 0.9.6

1. fixed a problem which cannot view images in the media window.

### 0.9.5

1. fixed a problem which cannot write tweets.
2. fixed a problem which back navigation button isn't appear.
    
### 0.9.4

1. Resolved a problem which can�t view some images

### 0.9.3

1. Fixed a problem which side bar buttons don't appear in some accounts

### 0.9.2

1. Sidebar buttons act more conveniently
    1. Goes the front page when click sidebar buttons again
    1. Scrolls to top when click sidebar buttons in the front page

## Download
https://bitbucket.org/groosoft/tweetssimply_release/downloads/TweetsSimply0_9_6.zip

[^1]:
    The icon of Tweets Simply is from FxEmojis (c) by Mozilla.  
    FxEmojis is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).